package utilitaires;

public class UtilDojo {
      
   public  static  String[] categories    = { "super-légers","mi-légers","légers",
                                              "mi-moyens","moyens",
                                              "mi-lourd","lourds"
                                            };
  
   public static String determineCategorie(String sexe, int poids){
     
       return categories[determineIndiceCategorie(sexe,poids)];       
   }  
   

   public static int    determineIndiceCategorie(String sexe, int poids){

      return (  sexe.equals("M") )? chercheIndice( poids, limitesHommes ) : chercheIndice( poids, limitesFemmes );
   }  
   
   public static  void  afficher(String pSexe){
       
       int      indiceMin = 0, indiceMax = categories.length-1;
          
       System.out.printf("%-15s Jusqu'à      %3d  kg\n",
            
           categories[indiceMin],(pSexe.equals("M")) ? limitesHommes[indiceMin]-1:limitesFemmes[indiceMin]-1
       );
        
       for (int i=1; i< categories.length-1;i++){
              
           int limiteSup = (pSexe.equals("M")) ? limitesHommes[i]-1:limitesFemmes[i]-1;
           int limiteInf = (pSexe.equals("M")) ? limitesHommes[i-1]:limitesFemmes[i-1];  
               
           System.out.printf("%-15s de  %d  kg à %3d  kg\n",categories[i],limiteInf,limiteSup);     
       }
      
       System.out.printf("%-15s A partir de  %3d  kg\n",
               
            categories[indiceMax],(pSexe.equals("M")) ? limitesHommes[indiceMax-1]:limitesFemmes[indiceMax-1]         
       );
   }
    
   //<editor-fold defaultstate="collapsed" desc="RESTE DU CODE">  
   
   private static int    chercheIndice(int poids, int[] tableauLimites ){
   
      int i;
      
      for(i=0;i<tableauLimites.length;i++)
      {         
         if( poids<tableauLimites[i] )
         {  
            break;
         }
      }
   
      return i;
   }  
   
   
   
   static  int[]    limitesHommes = {60, 66, 73, 81, 90, 100};
   static  int[]    limitesFemmes = {48, 52, 57, 63, 70, 78};
   
   //</editor-fold>

}
   



