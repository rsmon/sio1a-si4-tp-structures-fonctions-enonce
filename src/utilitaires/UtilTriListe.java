package utilitaires;

import comparateurs.ComparateurPersonnesParNbVictoiresDecroissant;
import comparateurs.ComparateurPersonnesParNom;
import comparateurs.ComparateurPersonnesParNomPrenom;
import comparateurs.ComparateurPersonnesParPoids;
import comparateurs.ComparateurPersonnesParSexeNomPrenom;
import static java.util.Collections.reverse;
import static java.util.Collections.sort;
import java.util.LinkedList;
import java.util.List;
import leclubdejudo.Personne;

public class UtilTriListe {
    
    public static void trierLesPersonnesParNomPrenom(List<Personne> lp){
          sort(lp, new ComparateurPersonnesParNomPrenom());
    }
    
    public static  void trierLesPersonnesParSexeNomPrenom(List<Personne> lp){
          sort(lp, new ComparateurPersonnesParSexeNomPrenom());
    }
    
    public static  void trierLesPersonnesParPoids(List<Personne> lp){
          sort(lp, new ComparateurPersonnesParPoids());
    }
    
    public static  void trierLesPersonnesParNom(List<Personne> lp){
          sort(lp, new ComparateurPersonnesParNom());
    }
    
    public static  void trierLesPersonnesParNbVictoiresDecroissant(List<Personne> lp){
          
          sort(lp, new ComparateurPersonnesParNbVictoiresDecroissant());
          reverse(lp);
    }  

    public static List<Personne> teteDeListe(List<Personne> liste, int nb){
      
      List<Personne> resultat= new LinkedList<Personne>(); 
      int i=0;
      for( Personne elem : liste){
          resultat.add(elem);
          i++;
          if( i==nb) break;
      }
      return resultat;
    
    }

}


