
package programmes;


import leclubdejudo.ClubJudo;
import leclubdejudo.Personne;
import utilitaires.UtilDate;
import utilitaires.UtilTriListe;


public class Exemple01 {

  public static void main(String[] args) {
        
    System.out.println("\n Liste des membres du Club\n");
        
    String format=" %-10s %-10s %-2s %-10s %4d kg %-20s %2d Victoires\n"; 
   
    UtilTriListe.trierLesPersonnesParNom(ClubJudo.listeDesMembres);
    
    for(Personne pers : ClubJudo.listeDesMembres){
            
      System.out.printf(format,
              
        pers.nom,
        pers.prenom,
        pers.sexe,
        UtilDate.formate(pers.dateNaiss),
        pers.poids,
        pers.ville,
        pers.nbVictoires
      );        
    } 
    
    System.out.println();
   }
}
