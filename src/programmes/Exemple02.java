package programmes;

import leclubdejudo.ClubJudo;
import leclubdejudo.Personne;
import utilitaires.UtilDate;
import utilitaires.UtilDojo;
import utilitaires.UtilTriListe;

public class Exemple02 {

  public static void main(String[] args) {
     
    String aujourdhui = UtilDate.aujourdhuiChaine();  
    System.out.printf("\n Liste des membres du Club agés de 22 ans au: %10s\n\n",aujourdhui);
        
    String format=" %-10s %-10s %-10s %-20s\n";
     
    UtilTriListe.trierLesPersonnesParNom(ClubJudo.listeDesMembres);
    
    for(Personne pers : ClubJudo.listeDesMembres){
       
        int agePers = UtilDate.ageEnAnnees(pers.dateNaiss);
      
        if( agePers==22){ 
           
           String categPers       = UtilDojo.determineCategorie(pers.sexe, pers.poids);      
           String dateNaiss       = UtilDate.formate(pers.dateNaiss); 
      
           System.out.printf(format,pers.prenom,pers.nom, dateNaiss, categPers);        
        }  
    } 
    
    System.out.println();
   }
}



