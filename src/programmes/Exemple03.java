
package programmes;

public class Exemple03 {

   
    public static void main(String[] args) {
        
        System.out.println("Catégories Hommes\n");    
        utilitaires.UtilDojo.afficher("M");
        System.out.println(); 
            
        
        System.out.print("Catégorie pour un homme de 81 kg: ");   
        System.out.println(utilitaires.UtilDojo.determineCategorie("M", 81));
        System.out.println();
        
        System.out.println("Catégories Femmes\n");    
        utilitaires.UtilDojo.afficher("F");
        System.out.println(); 
        
        
        System.out.print("Catégorie pour une femme de 60 kg: ");   
        System.out.println(utilitaires.UtilDojo.determineCategorie("F", 57));
        System.out.println();
        
    }
}
