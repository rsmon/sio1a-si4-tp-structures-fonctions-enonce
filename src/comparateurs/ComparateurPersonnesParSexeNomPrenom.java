
package comparateurs;

import java.util.Comparator;
import leclubdejudo.Personne;

public class ComparateurPersonnesParSexeNomPrenom implements Comparator{

    @Override
    public int compare(Object t, Object t1) {
        
        int resultat=0;
       
        Personne p  = (Personne)  t ;
        Personne p1 = (Personne)  t1;
            
        //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par sexe/nom/prénom">
           
        resultat= p.sexe.compareTo(p1.sexe);
        if ( resultat==0){
             resultat=  p.nom.compareTo(p1.nom);
             if (resultat==0) resultat= p.prenom.compareTo(p1.prenom);
         }            
        
        //</editor-fold>
      
        return resultat;  
    }    
}
